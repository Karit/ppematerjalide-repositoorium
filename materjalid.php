<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Digitaalsete õppevahendite repositoorium</title>
		<link rel="stylesheet" type="text/css" href="style.css">
		 <script src="scripts.js"></script>
	</head>
	<body>
		<p id="header">
				<button type="button" onclick= "avalehele()">Avaleht</button>
				<button type="button" onclick= "materjaleOtsima()">Otsi õppematerjale!</button>
		</p>
		<div id="banner">
			<img src="banner.jpg" alt="banner">
		</div>
		<h3> Siia saad lisada materjale! </h3>
		<form action="materjalid.php" method="post" enctype="multipart/form-data">
  		Materjali nimi:<br>
  		<input type="text" name="failinimi" placeholder=""><br>
  		Materjali autor:<br>
  		<input type="text" name="autor" placeholder=""><br>
  		Materjali tüüp:<br>
  		 <select name="tyyp">
  			<option value="tööleht">Tööleht</option>
  			<option value="esitlus">Esitlus</option>
 			<option value="test">Test</option>
 			<option value="pilt">Pilt</option>
 			<option value="audio">Helifail</option>
		</select> <br>
 		Lisa materjal:<br>
 		 <input type="file" value="Vali materjal" name="nimi">
 		 <input type="submit" value="Lisa" name="Lisa">
		</form>

		<div id="footer">Rõõmsat Kasutamist!</div>
		

<?php

  include("action-materjalid.php");

?>


	</body>
</html>

