<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Digitaalsete õppevahendite repositoorium</title>
		<link rel="stylesheet" type="text/css" href="style.css">
		 <script src="scripts.js"></script>
	</head>
	<body>
		<p id="header">
				<button onclick= "materjalideJuurde()">Lisa õppematerjale!</button>
					<button type="button" onclick= "materjaleOtsima()">Otsi õppematerjale!</button>
		</p>
		<div id="banner">
			<img src="banner.jpg" alt="banner">
		</div>
		<pre id="tutvustus">
			<br>
			<br>
		  Hea õpetaja!
		  <br>
		  Siin repositooriumis asuvad õpetajate õppematerjalid.
		  <br>
		  Saad siin neid lisada ja otsida!
		  <br>
		  
		</pre>
		<div id="footer">Rõõmsat kasutamist!</div>
	</body>
</html>